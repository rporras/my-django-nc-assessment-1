# -*- coding: utf-8 -*-
from django.urls import reverse
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Movie(models.Model):
    title = models.CharField(_('Movie\'s title'), max_length=255)
    year = models.PositiveIntegerField(default=2019)
    # Example: PG-13
    rated = models.CharField(max_length=64)
    released_on = models.DateField(_('Release Date'))
    genre = models.CharField(max_length=255)
    director = models.CharField(max_length=255)
    plot = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    # Todo: add Rating models

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('movies:detail', kwargs={'pk': self.pk})

    def get_rating(self):
        return round(self.average_rating, 2) if self.average_rating else 'No rating yet'


class Rating(models.Model):
    BAD = 0
    NOT_GOOD = 1
    NEUTRAL = 2
    GOOD = 3
    VERY_GOOD = 4
    EXCELENT = 5
    RATES = (
        (BAD, _('Bad movie')),
        (NOT_GOOD, _('It is better not watch it')),
        (NEUTRAL, _('It is not good but it is not bad either')),
        (GOOD, _('Somehow good')),
        (VERY_GOOD, _('Good to see it')),
        (EXCELENT, _('Recommended')),
    )
    movie = models.ForeignKey(Movie, on_delete=models.DO_NOTHING)
    value = models.IntegerField(choices=RATES, default=BAD)
    comment = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('movies:detail', kwargs={'pk': self.movie.pk})
